// ====================================================================
// This file is part of the Lattice Algebra Toolbox for Scilab 5.x
// Copyright (C) Grupo de Inteligencia Computacional, Universidad del País Vasco (UPV/EHU), Spain
// released under the terms of the GNU General Public License
//
// Lattice Algebra Toolbox is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Lattice Algebra Toolbox is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Lattice Algebra Toolbox. If not, see <http://www.gnu.org/licenses/>.
//
// Description: Return the vertices of the convex polytope containing the set of real vectors valA.
//
// see Lattice Algebra Toolbox (help)
// ====================================================================
function [s] = lat_convexpolytope(valA)
  [u,v] = lat_maxmincorners(valA);
  [W,M] = lat_laam(valA);
  [r_w,c_w] = size(W);
  for i=1:c_w
    W(:,i) = W(:,i) + u(i);
    M(:,i) = M(:,i) + v(i);
  end
  s = zeros(r_w,2*c_w+2);
  s(:,1:c_w) = W;
  s(:,c_w+1:2*c_w) = M;
  s(:,2*c_w+1) = u;
  s(:,2*c_w+2) = v;
endfunction
// ====================================================================
