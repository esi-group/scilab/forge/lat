;##############################################################################################################
; Inno Setup Install script for Toolbox_skeleton
; http://www.jrsoftware.org/isinfo.php
; Allan CORNET
; This file is released into the public domain
;##############################################################################################################
; modify this path where is toolbox_skeleton directory
#define BinariesSourcePath "C:\Programs files\scilab-5.0\contrib\lattice_algebra_toolbox"
;
#define Lattice_Algebra_version "0.2"
#define CurrentYear "2011"
#define Lattice_Algebra_toolboxDirFilename "lattice_algebra_toolbox"
;##############################################################################################################
[Setup]
; Debut Donn�es de base � renseigner suivant version
SourceDir={#BinariesSourcePath}
AppName=Lattice Algebra Toolbox
AppVerName=Lattice Algebra Toolbox version 0.1
DefaultDirName={pf}\{#Lattice_Algebra_toolboxDirFilename}
InfoAfterfile=readme.txt
LicenseFile=license.txt
WindowVisible=true
AppPublisher=Grupo de Inteligencia Computacional, Universidad del Pais Vasco, Spain
BackColorDirection=lefttoright
AppCopyright=Copyright � {#CurrentYear}
Compression=lzma/max
InternalCompressLevel=normal
SolidCompression=true
VersionInfoVersion={#Lattice_Algebra_version}
VersionInfoCompany=Grupo de Inteligencia Computacional, Universidad del Pais Vasco, Spain
;##############################################################################################################
[Files]
; Add here files that you want to add
Source: loader.sce; DestDir: {app}
Source: etc\lattice_algebra_toolbox.quit; DestDir: {app}\etc
Source: etc\lattice_algebra_toolbox.start; DestDir: {app}\etc
Source: macros\buildmacros.sce; DestDir: {app}\macros
Source: macros\lib; DestDir: {app}\macros
Source: macros\names; DestDir: {app}\macros
Source: macros\*.sci; DestDir: {app}\macros
Source: macros\*.bin; DestDir: {app}\macros
Source: tests\*.*; DestDir: {app}\tests; Flags: recursesubdirs
;Source: includes\*.h; DestDir: {app}\includes; Flags: recursesubdirs
;Source: locales\*.*; DestDir: {app}\locales; Flags: recursesubdirs
Source: demos\*.*; DestDir: {app}\locales; Flags: recursesubdirs
;
;##############################################################################################################
