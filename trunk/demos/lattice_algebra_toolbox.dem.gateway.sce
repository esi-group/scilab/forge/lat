// ====================================================================
// Copyright INRIA 2008
// Allan CORNET
// This file is released into the public domain
// ====================================================================
demopath = get_absolute_file_path("lattice_algebra_toolbox.dem.gateway.sce");

subdemolist = [	"demo lat_conjugate",		"lat_conjugate.dem.sce"; ..
		"demo lat_pointwisemaxt",	"lat_pointwisemax.dem.sce"; ..
		"demo lat_pointwisemin",	"lat_pointwisemin.dem.sce"; ..
		"demo lat_maxproduct",		"lat_maxproduct.dem.sce"; ..
		"demo lat_minproduct",		"lat_minproduct.dem.sce"; ..
		"demo lat_minimaxsum",		"lat_minimaxsum.dem.sce"; ..
		"demo lat_minfixedpoint",	"lat_minfixedpoint.dem.sce"; ..
		"demo lat_maxfixedpoint",	"lat_maxfixedpoint.dem.sce"; ..
		"demo lat_laam",		"lat_laam.dem.sce"; ..
		"demo lat_lham",		"lat_lham.dem.sce"; ..
		"demo lat_isdependent",		"lat_isdependent.dem.sce"; ..
		"demo lat_maxmincorners",	"lat_maxmincorners.dem.sce"; ..
		"demo lat_convexpolytope",	"lat_convexpolytope.dem.sce";];

subdemolist(:,2) = demopath + subdemolist(:,2);
// ====================================================================
