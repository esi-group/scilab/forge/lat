;##############################################################################################################
; Script d'installation Inno Setup (5.2.2) pour scilab
; Allan CORNET
; Version TRUNK
; This file is released into the public domain
;##############################################################################################################
;--------------------------------------------------------------------------------------------------------------
; toolbox_skeleton
;--------------------------------------------------------------------------------------------------------------
#define LATTICE_ALGEBRA_TOOLBOX "lattice_algebra_toolbox"
;
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\builder.sce; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\toolbox_skeleton_redist.iss; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\changelog.txt; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\license.txt; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\readme.txt; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
;
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\demos\*.*; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\demos; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\etc\toolbox_skeleton.quit; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\etc; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\etc\toolbox_skeleton.start; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\etc; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\help\builder_help.sce; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\help; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}

Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\help\en_US\build_help.sce; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\help\en_US; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\help\en_US\*.xml; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\help\en_US; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\help\fr_FR\build_help.sce; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\help\fr_FR; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\help\fr_FR\*.xml; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\help\fr_FR; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
;
;Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\includes\*.h; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\includes
;Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\locales\*.*; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\locales
;
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\macros\buildmacros.sce; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\macros; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\macros\cleanmacros.sce; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\macros; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\macros\*.sci; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\macros; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\sci_gateway\builder_gateway.sce; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\sci_gateway; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\sci_gateway\c\builder_gateway_c.sce; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\sci_gateway\c; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\sci_gateway\c\sci_csum.c; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\sci_gateway\c; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\sci_gateway\c\sci_csub.c; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\sci_gateway\c; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\sci_gateway\cpp\sci_cpp_find.cxx; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\sci_gateway\cpp; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\sci_gateway\cpp\builder_gateway_cpp.sce; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\sci_gateway\cpp; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\sci_gateway\fortran\builder_gateway_fortran.sce; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\sci_gateway\fortran; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\sci_gateway\fortran\sci_fsum.c; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\sci_gateway\fortran; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\src\builder_src.sce; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\src; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\src\c\builder_c.sce; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\src\c; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\src\c\csum.c; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\src\c; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\src\c\*.h; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\src\c; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\src\c\csub.c; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\src\c; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\src\fortran\builder_fortran.sce; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\src\fortran; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\src\fortran\fsum.f; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\src\fortran; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
Source: contrib\{#LATTICE_ALGEBRA_TOOLBOX}\tests\*.*; DestDir: {app}\contrib\{#LATTICE_ALGEBRA_TOOLBOX}\tests; Flags: recursesubdirs; Components: {#COMPN_LATTICE_ALGEBRA_TOOLBOX}
;--------------------------------------------------------------------------------------------------------------
