The Lattice Algebra Toolbox (LAT) is a set of Lattice Algebra operators and computational methods based on Lattice Algebra for its use from Scilab.

Bibliographical references:
* "Lattice algebra approach to endmember determination in hyperspectral imgagery", G.X. Ritter and G. Urcid. Advances in Imaging and Electron Physics, Volume 160, ISSN 1076-5670. 2010.
* "Two lattice computing approaches for the unsupervised segmentation of hyperspectral images". M. Graña, I. Villaverde, J.O. Maldonado and C. Hernández. Neurocomputing 72(10-12), pp: 2111-2120. 2009.
* "Autonomous single-pass endmember approximation using lattice auto-associative memories". G.X. Ritter, G. urcid and M.S Schmalz. Neurocomputing 72(10-12), pp: 2101-2110. 2009.
* "Fixed points of lattice transforms and lattice associative memories". G.X. Ritter and P. Gader. Adv. Imaging and Electr. Phys. 144, pp: 165-242. 2006.
* "A single individual evolutionary strategy for endmember search in hyperspectral images". M. Graña, C. Hernández and J. Gallego. Information Science 161(3-4), pp:181-197. 2004.
* "Associative morphological memories for endmember determination in spectral unmixing". M. Graña, P. Sussner and G.X. Ritter. Proceedings of IEEE, International Conference on Fuzzy Systems, pp:1285-1290. 2003.
* "Morphological associative memories". G.X. ritter, P. Sussner and J.L. Díaz de León. IEEE Trans. Neural Netw. 9(2), pp: 281-293. 1998.

Copyright (C) Grupo de Inteligencia Computacional, Universidad del País Vasco (UPV/EHU), Spain released under the terms of the GNU General Public License.
Lattice Algebra Toolbox is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
Lattice Algebra Toolbox is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.
You should have received a copy of the GNU General Public License along with Lattice Algebra Toolbox. If not, see <http://www.gnu.org/licenses/>.

 -- Miguel Angel Veganzones <miguelangel.veganzones@ehu.es>
 -- Grupo de Inteligencia Computacional <http://www.ehu.es/computationalintelligence>
 -- Universidad del País Vasco (UPV/EHU), Spain
 -- Mon, 21 Feb 2011
